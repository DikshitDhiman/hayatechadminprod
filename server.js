//Install express server
const express = require('express');
const app = express();
const path = require('path');
const fs = require('fs');

const port = process.env.PORT || 4200;

// Serve only the static files form the dist directory
app.use(express.static(__dirname + '/dist/universal'));

//const https = require('https');
const http = require('http');

app.get('/*', function (req, res) {
  return res.sendFile(path.join(__dirname + '/dist/universal', 'index.html'));
})

// const privateKey = fs.readFileSync('/home/jenkins/workspace/SSL_Free_24Jan2019/meanstack_stagingsdei_com.key', 'utf8');
// const certificate = fs.readFileSync('/home/jenkins/workspace/SSL_Free_24Jan2019/meanstack_stagingsdei_com.crt', 'utf8');
// //var ca = fs.readFileSync('/home/ubuntu/SSL_Free/meanstack_stagingsdei_com_cabundle.crt');
// const httpsOptions = { key: privateKey, cert: certificate };

const server = http.createServer(app);
//const server = https.createServer(httpsOptions, app);


// Start the app by listening on the default Heroku port
server.listen(port, (err, succ) => {
  if (err) {
    console.log('Error : ', err);
  } else {
    console.log('Express server listening on port ' + port);
  }
});
